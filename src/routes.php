<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {

    try {
        $params = $request->getQueryParams();
        if (!empty($params['filtr'])) {
            //filtruju
            $stmt = $this->db->prepare('SELECT * FROM person WHERE first_name ILIKE :f OR last_name ILIKE :f ORDER BY last_name');

            $stmt->bindValue(':f', '%' . $params['filtr'] . '%');
            $stmt->execute();
        } else {
            //nefiltruju
            $stmt = $this->db->query('SELECT * FROM person');
        }
        $osoby = $stmt->fetchAll();
        // Render index view
        return $this->view->render($response, 'index.latte', ['osoby' => $osoby]);
    } catch (Exception $ex) {
        //zalogovat chyby
        $this->logger->error($ex->getMessage());
        //ukoncit obsluhu a vypsat
        exit('Chyba DB, mrkni do logu!');
    }

})->setName('index');


$app -> get('/pridej', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'new_person.latte');
});

$app -> post('/pridej', function (Request $request, Response $response, $args){
    $data = $request ->getParsedBody();
    try {
        if (!empty($data['jm'] && !empty($data['pr']) && !empty($data['pz']))) {
        $vy = empty($data['vy']) ? null : $data['vy'];
        $dn = empty($data['dn']) ? null : $data['dn'];
            
     $stmt = $this->db->prepare(
             'INSERT INTO person(first_name, last_name, nickname, birth_day, height, gender) VALUES (:jm, :pr, :pz, :dn, :vy, :po)');   
     $stmt->bindValue(':jm', $data['jm']);
     $stmt->bindValue(':pr', $data['pr']);
     $stmt->bindValue(':pz', $data['pz']);
     $stmt->bindValue(':po', $data['po']);
     $stmt->bindValue(':dn', $dn);
     $stmt->bindValue(':vy', $vy);
     $stmt -> execute();
     //presmerovat na index(vypis osob)
     return $response->withAddedHeader('Location', '.');
        } else { //chyba, znovu zobrazit formular
            $tplVars['error']='Nejsou zadany povinne udaje';
        }
        
        } catch (Exception $ex){
            if ($ex->getCode() == 23505) { //duplicita
                $tplVars['error']='Duplicitni udaje';
            } elseif ($ex->getCode() == 22007) { //spatny datum
                $tplVars['error']='Spatny format data';
            } else{
                $this->logger->error($ex->getMessage());
                exit('Chyba DB');
            }         
                        
    }
    return $this->view->render($response, 'new_person.latte', $tplVars);
});

